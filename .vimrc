" My .vimrc file
" I am wanting to use ranger as my daily driver and will need
" a few things before I can work properly with it. Below are 
" all the things I want to configure to be able to program
" solvers in OpenFOAM and python primarily.
"------------------------------------------------------------------------------
" Set 'nocompatible' to ward off unexpected things the distro might want
" to override
set nocompatible

filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
Plugin 'plasticboy/vim-markdown'
Plugin 'Valloric/YouCompleteMe'
Plugin 'scrooloose/syntastic'
Plugin 'lervag/vim-foam'

" All of your Plugins must be added before the following line
call vundle#end()            " required

" Determine the type of file from extension
filetype indent plugin on

" Enable syntax highlighting
syntax on

" Display line numbers
:set number

" Allow hidden, meaning that you can switch to new buffers without saving
" the current buffer
set hidden

" Allow better cmd completion
set wildmenu

" Show partial cmd in last line of screen
set showcmd

" Highlight searches (using C-L) to temporarily turn off highlighting
set hlsearch

" Use case insensitive search, except when using caps
set ignorecase
set smartcase

" Allow to backspace over autoindent
set backspace=indent,eol,start

" When opening new line, keep same indent as the current line
set autoindent

" Stop certaim movements from going to first character of a line
set nostartofline

" Display cursor position on last line of screen
set ruler

" Always display the status line, even if only 1 window is displayed
set laststatus=2

" Use visual bell instead of annoying beep
set visualbell

" Reset the terminal code for visual bell.
set t_vb=

" Enable use of mouse for all modes
set mouse=a

" Set command window height to 2 lines
set cmdheight=2

" Quickly timeout on keycodes, but never on mappings
set notimeout ttimeout ttimeoutlen=200

" Use F11 to toggle betn paste and nopaste
set pastetoggle=<F11>

set encoding=utf-8

" Indentation settings
"------------------------------------------------------------------------------
set shiftwidth=4
set softtabstop=4
set tabstop=4
set expandtab

set clipboard=unnamedplus
set guioptions+=a
vmap <leader>y :w! /tmp/vitmp<CR>                                                                   
nmap <leader>p :r! cat /tmp/vitmp<CR>
